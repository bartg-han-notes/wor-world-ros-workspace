#include "tutorial_nodes/basic_node.hpp"

int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<BasicNode>());
    rclcpp::shutdown();
}